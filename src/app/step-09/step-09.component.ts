import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';

/* HTTP Client to retrieve data */
import { HttpClient, HttpRequest, HttpEvent, HttpEventType } from '@angular/common/http';

/* Data Service */
import { DataService } from '../data.service';

/* Angular Material */
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-step-09',
  templateUrl: './step-09.component.html',
  styleUrls: ['./step-09.component.scss']
})
export class Step09Component implements OnInit, OnDestroy, AfterViewInit {

  // Setting the FlowStep
  flowStep = '8';
  flowStepResults: any= {};
  getFlowStepNumber: string;

  // Survey Data and Variables
  surveyResults: any = {};
  preReg: string;
  pickup: string;
  years: string;
  jersey: string;
  Vegetarian: string;

  shuttle1: string;
  shuttle2: string;
  shuttle3: string;
  shuttle4: string;
  shuttle5: string;
  shuttle6: string;
  shuttle7: string;
  shuttle8: string;
  shuttle9: string;

  shuttleQ1: string;
  shuttleQ2: string;
  shuttleQ3: string;
  shuttleQ4: string;
  shuttleQ5: string;
  shuttleQ6: string;
  shuttleQ7: string;
  shuttleQ8: string;
  shuttleQ9: string;

  reserve: string = 'false';
  reserve2: string = 'false';
  reserve3: string = 'false';
  reserve4: string = 'false';
  reserve5: string = 'false';
  reserve6: string = 'false';
  reserve7: string = 'false';
  reserve8: string = 'false';
  reserve9: string = 'false';


  // Registration Data
  regData: any = {};

  // Tentstatus Variable
  tentStatus: string;

  // Check-in Status Data
  updateRegRes: any = {};

  // Variable for Timeout Function
  timeOut: any;
  timeOut2: any;

  constructor(public data: DataService,
              private http: HttpClient,
              private router: Router,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getSurveyRes();
    window.scrollTo(0, 0);

    // Setting a timeout function to log inactive users out (for privacy protection)
    this.timeOut = setTimeout(() => {

      this.snackBar.open('Need more time? For your security, you\'ve been logged out of your check-in session. To continue your online check-in, simply return to the login screen.', 'Close', {
        duration: 15000,
        extraClasses: ['error-info']
      });

      this.timeOut2 = setTimeout(() => {
        this.data.logOut();
      }, 240000);
    }, 858000);

    // Checking logged in state, and running correct functions
    if (this.data.isLoggedIn() === true && this.data.tokenExpired === false) {

      // If user is logged in, then get the current flowStep (to prevent people from skipping pages)
      this.getFlowStep();
    } else if (this.data.storageToken === undefined) {
      // console.log('Auth Token Expired.');

      this.snackBar.open('Login session expired, please login again.', 'Close', {
        duration: 3500,
        extraClasses: ['error-info']
      });

      this.router.navigate(['/step-01']);

    } else {
      // if not logged in, go back to step 1 (login page)
      this.snackBar.open('You are not logged in, please login.', 'Close', {
        duration: 3500,
        extraClasses: ['error-info']
      });

      this.router.navigate(['/step-01']);
    }

  }

  ngAfterViewInit() {}

  // Clear the timeout function upon entering a new route
  ngOnDestroy() {
    clearTimeout(this.timeOut);
  }

  // Get the Survey Responses
  getSurveyRes() {
    this.data.method = 'CRTeamraiserAPI?method=getSurveyResponses&api_key=cfrca&v=1.0&fr_id=' + this.data.eventID + (this.data.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.data.ssoToken + '&survey_id=' + this.data.surveyID + '&response_format=json';
    this.http.post(this.data.convioURL + this.data.method, null)
      .subscribe(res => {
        this.surveyResults = res;
        console.log(this.surveyResults);

        // Accepted Upsell Offer (for Pre-register question)
        for (const result of this.surveyResults.getSurveyResponsesResponse.responses) {
          
          if (result.questionId === this.data.question6) {
            if (result.responseValue === '[object Object]') {
              this.preReg = 'No'
            } else {
              this.preReg = result.responseValue;
            }
          }

          if (result.questionId === this.data.question32) {
            this.pickup = result.responseValue;
          }

          if (result.questionId === this.data.question1) {
            this.years = result.responseValue;
          }

          if (result.questionId === this.data.question18) {
            this.jersey = result.responseValue;
          }

          if (result.questionId === this.data.question15) {
            this.Vegetarian = result.responseValue;
          }

          if (result.questionId === this.data.question20) {
            this.shuttleQ1 = result.questionText;
            this.shuttle1 = result.responseValue
            if (result.responseValue !== "I don't need a shuttle" && this.data.participationType !== 'Crew') {
              this.reserve = 'true';  
            }
          }
          if (result.questionId === this.data.question21) {
            console.log(result.questionText)
            console.log(result.responseValue)
            this.shuttleQ2 = result.questionText;
            this.shuttle2 = result.responseValue
            if (result.responseValue !== "I don't need a shuttle") {
              this.reserve2 = 'true';  
            } 
          }
          if (result.questionId === this.data.question22) {
            this.shuttleQ3 = result.questionText;            
            this.shuttle3 = result.responseValue
            if (result.responseValue !== "I don't need a shuttle" && this.data.participationType !== 'Crew') {
              this.reserve3 = 'true';  
            }
          }
          if (result.questionId === this.data.question23) {
            this.shuttleQ4 = result.questionText;
            this.shuttle4 = result.responseValue
            if (result.responseValue !== "I don't need a shuttle") {
              this.reserve4 = 'true';  
            }
          }
          if (result.questionId === this.data.question24) {
            this.shuttleQ5 = result.questionText;
            this.shuttle5 = result.responseValue
            if (result.responseValue !== 'No') {
              this.reserve5 = 'true';  
            }
          }
          if (result.questionId === this.data.question25) {
            this.shuttleQ6 = result.questionText;
            this.shuttle6 = result.responseValue
            if (result.responseValue !== "I don't need a shuttle") {
              this.reserve6 = 'true';  
            } 
          }
          if (result.questionId === this.data.question26) {
            this.shuttleQ7 = result.questionText;            
            this.shuttle7 = result.responseValue
            if (result.responseValue !== "I don't need a shuttle") {
              this.reserve7 = 'true';  
            }
          }
          if (result.questionId === this.data.question27) {
            this.shuttleQ8 = result.questionText;
            this.shuttle8 = result.responseValue
            if (result.responseValue !== "I don't need a shuttle") {
              this.reserve8 = 'true';  
            }
          }
          if (result.questionId === this.data.question28) {
            this.shuttleQ9 = result.questionText;
            this.shuttle9 = result.responseValue
            if (result.responseValue !== "I don't need a shuttle") {
              this.reserve9 = 'true';  
            }
          }
        }

      }, (err) => {
        // console.log(err);
      });
  }

  // Update the current Flowstep
  updateFlowStep() {
    this.data.method = 'CRTeamraiserAPI?method=updateRegistration&api_key=cfrca&v=1.0&fr_id=' + this.data.eventID + (this.data.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.data.ssoToken + '&flow_step=' + this.flowStep + '&response_format=json';
    this.http.post(this.data.convioURL + this.data.method, null)
      .subscribe(res => {
        // console.log('Flow step updated.')
      }, (err) => {
        if (err) {
          // console.log('There was an error updating the flowstep.');
        }
      });
  }

  // Set checkInStatus as Complete
  updateCheckInStatusComplete() {
    this.data.method = 'CRTeamraiserAPI?method=updateRegistration&api_key=cfrca&v=1.0' + '&fr_id=' + this.data.eventID + (this.data.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.data.ssoToken + '&checkin_status=complete' + '&response_format=json';
    this.http.post(this.data.convioURL + this.data.method, null)
      .subscribe(res => {
        this.updateRegRes = res;
        // console.log(this.updateRegRes);
        // window.location.reload();
      });
  }

  // Get the current Flowstep
  getFlowStep() {
    const token = localStorage.getItem('token');
    this.data.method = 'CRTeamraiserAPI?method=getFlowStep&api_key=cfrca&v=1.0&response_format=json&fr_id=' + this.data.eventID + (this.data.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + token;
    this.http.post(this.data.convioURL + this.data.method, null)
      .subscribe(res => {
        this.flowStepResults = res;
        this.getFlowStepNumber = this.flowStepResults.getFlowStepResponse.flowStep;

        // Checking the participants flow step to prevent user from skipping a flowstep
        if (this.getFlowStepNumber === this.flowStep) {

          // If the flow step matches to where they are supposed to be, then run the functions for the current route below
          this.updateFlowStep();
          this.updateCheckInStatusComplete();
          this.getSurveyRes();
          this.data.getUserInfo();
          this.data.getRegInfo();
          this.data.getTeam();
        } else {

          // If flowstep does not match, show error message and send them back to the previous page/flowstep.
          this.snackBar.open('You have been redirected to your previously saved location.', 'Close', {
            duration: 3500,
            extraClasses: ['routing-info']
          });

          // Check the Flowstep, if matched, send them to the proper route
          if (this.getFlowStepNumber === '0') {
            this.router.navigate(['/step-02']);
          }
          if (this.getFlowStepNumber === '1') {
            this.router.navigate(['/step-02']);
          }
          if (this.getFlowStepNumber === '2') {
            this.router.navigate(['/step-03']);
          }
          if (this.getFlowStepNumber === '3') {
            this.router.navigate(['/step-04']);
          }
          if (this.getFlowStepNumber === '4') {
            this.router.navigate(['/step-05']);
          }
          if (this.getFlowStepNumber === '5') {
            this.router.navigate(['/step-06']);
          }
          if (this.getFlowStepNumber === '6') {
            this.router.navigate(['/step-07']);
          }
          if (this.getFlowStepNumber === '7') {
            this.router.navigate(['/step-08']);
          }
          if (this.getFlowStepNumber === '8') {
            this.router.navigate(['/step-09']);
          }
        }

      }, (err) => {
        // console.log(err);

        // If flowstep has error, log out the user (to prevent API errors)
        this.data.logOut();
      });
  }

  // Print Method
  print() {
    window.print();
  }

}
