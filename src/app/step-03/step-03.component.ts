import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType } from '@angular/common/http';
import { ErrorStateMatcher } from '@angular/material/core';

import { DataService } from '../data.service';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-step-03',
  templateUrl: './step-03.component.html',
  styleUrls: ['./step-03.component.scss']
})
export class Step03Component implements OnInit, OnDestroy, AfterViewInit  {

  step03Form: FormGroup;

  matcher = new MyErrorStateMatcher();

  buttonStatus:boolean = true;

  remember:boolean = false;

  getInfo: any = {};

  // Flowstep
  flowStep = '2';
  flowStepResults: any = {};
  getFlowStepNumber: string;

  // Results from HTTP calls set as Objects for OOP
  surveyResults: any = {};
  regRes: any = {};

  // DOM Element Responses from Profile
  pCancerRes: string;
  pVegRes: string;
  pJerseyRes: string;

  // DOM Element Responses
  cancerRes: string;
  packetRes: string;
  vegRes: string;
  shuttleRes: string;
  shuttleRes2: string;
  shuttleRes3: string;
  // shuttleRes4: string;
  // shuttleRes5: string;
  // shuttleRes6: string;
  // shuttleRes7: string;
  // shuttleRes8: string;
  // shuttleRes9: string;
  bikeRes1: string;
  bikeRes2: string;
  bikeRes3: string;
  jerseyRes: string;
  attendanceRes: string;
  routeRes: string;
  experiencedRiderRes: string;
  glutenRes: string;
  safetyRes: string;

  nameError:boolean = false;
  numError:boolean = false;
  yearError:boolean = false;
  cancerError:boolean = false;
  vegError:boolean = false;
  jerseyError:boolean = false;
  packetError:boolean = false;
  shuttle1Error:boolean = false;
  shuttle2Error:boolean = false;
  shuttle3Error:boolean = false;
  // shuttle4Error:boolean = false;
  // shuttle5Error:boolean = false;
  // shuttle6Error:boolean = false;
  // shuttle7Error:boolean = false;
  // shuttle8Error:boolean = false;
  // shuttle9Error:boolean = false;
  routeError:boolean = false;


  // Variable to hide elements
  hideMe: boolean = true;

  // Select Options for Yes/No
  matSelect = [
    {value: 'Yes', viewValue: 'Yes'},
    {value: 'No', viewValue: 'No'}
  ];



  // Select Options for shuttle question 1
  matSelectQ1 = [
    {value: "5:15 a.m. and 5:30 a.m. Departure:Host Hotel (Ramada Surrey) to the Start Line in Cloverdale.", viewValue: "5:15 a.m. and 5:30 a.m. Departure: Host Hotel (Ramada Surrey) to the Start Line in Cloverdale"},
    {value: "I don't need a shuttle", viewValue: "I don't need a shuttle"}
  ];


  // Select Options for shuttle question 2
  matSelectQ2 = [
    {value: "Saturday August 24th from Fairmont Waterfront: 4:05 a.m. and 4:10 a.m.", viewValue: "Saturday August 24th from Fairmont Waterfront: 4:05 a.m. and 4:10 a.m."},
    {value: "Saturday August 24th from Commercial Broadway 4:45 a.m. and 4:55 a.m." , viewValue: "Saturday August 24th from Commercial Broadway 4:45 a.m. and 4:55 a.m."},
    {value: "I don't need a shuttle" , viewValue: "I don't need a shuttle"}
  ];

  // Select Options for shuttle question 3
  // matSelectQ3 = [
  //   {value: "12 p.m.", viewValue: "12 p.m."},
  //   {value: "2 p.m.", viewValue: "2 p.m."},
  //   {value: "3 p.m.", viewValue: "3 p.m."},
  //   {value: "4 p.m.", viewValue: "4 p.m."},
  //   {value: "5 p.m.", viewValue: "5 p.m."},
  //   {value: "I don't need a shuttle", viewValue: "I don't need a shuttle"}
  // ];

  matSelectQ3 = [
    {value: "Shuttles depart at 12 p.m., 1 p.m., 2 p.m., 3 p.m., 4 p.m. and 5 p.m.", viewValue: "Shuttles depart at 12 p.m., 1 p.m., 2 p.m., 3 p.m., 4 p.m. and 5 p.m."},
    {value: "I don't need a shuttle", viewValue: "I don't need a shuttle"}
  ];
  

  safetySelect = [
    {value: 'Yes', viewValue: 'Yes'},
    {value: 'No', viewValue: 'No'}
  ];

  // Select Options for shuttle question 4
  matSelectPacket = [
    {value: 'Yes', viewValue: 'Yes (If selected, your Rider packet will be mailed to your home address)'},
    {value: 'No', viewValue: 'No'}
  ];

  routes = [
    {value: 'Classic Route', viewValue: 'Classic Route'},
    {value: 'Challenge Route', viewValue: 'Challenge Route'}
  ];

  // multiple selection

  noSelect = [
    {value: 'No', viewValue: 'No'}
  ];

  // Select Options for Jesey Sizes
  jerseySelect = [
    {value: 'Mens_XXS', viewValue: 'Mens_XXS'},
    {value: 'Mens_XS', viewValue: 'Mens_XS'},
    {value: 'Mens_SM', viewValue: 'Mens_SM'},
    {value: 'Mens_MD', viewValue: 'Mens_MD'},
    {value: 'Mens_LG', viewValue: 'Mens_LG'},
    {value: 'Mens_XL', viewValue: 'Mens_XL'},
    {value: 'Mens_2XL', viewValue: 'Mens_2XL'},
    {value: 'Mens_3XL', viewValue: 'Mens_3XL'},
    {value: 'Womens_XXS', viewValue: 'Womens_XXS'},
    {value: 'Womens_XS', viewValue: 'Womens_XS'},
    {value: 'Womens_SM', viewValue: 'Womens_SM'},
    {value: 'Womens_MD', viewValue: 'Womens_MD'},
    {value: 'Womens_LG', viewValue: 'Womens_LG'},
    {value: 'Womens_XL', viewValue: 'Womens_XL'},
    {value: 'Womens_2XL', viewValue: 'Womens_2XL'},
    {value: 'Womens_3XL', viewValue: 'Womens_3XL'}    
  ];

  // Years attended Options
  years = [
    {value: '1', viewValue: '1'},
    {value: '2', viewValue: '2'},
    {value: '3', viewValue: '3'},
    {value: '4', viewValue: '4'},
    {value: '5', viewValue: '5'},
    {value: '6', viewValue: '6'},
    {value: '7', viewValue: '7'},
    {value: '8', viewValue: '8'},
    {value: '9', viewValue: '9'},
    {value: '10', viewValue: '10'},
    {value: '11', viewValue: '11'},
    {value: '12', viewValue: '12'}
  ];

  // Specifying API Method Variable
  method: string;

  // Variable for Timeout
  timeOut: any;
  timeOut2: any;

  constructor(public dataService: DataService,
              public route: Router,
              public http: HttpClient,
              public snackBar: MatSnackBar) { }

  ngOnInit() {

    window.scrollTo(0,0);

    // Setting a timeout function to log inactive users out (for privacy protection)
    this.timeOut = setTimeout(() => {
      this.snackBar.open('Need more time? For your security, you\'ve been logged out of your check-in session. To continue your online check-in, simply return to the login screen.', 'Close', {
        duration: 15000,
        extraClasses: ['error-info']
      });

      this.timeOut2 = setTimeout(() => {
        this.dataService.logOut();
      }, 240000);
    }, 858000);

    this.step03Form = new FormGroup({
      yearsAttended: new FormControl(this.attendanceRes, Validators.required),
      packetPickup: new FormControl(this.packetRes, Validators.required),
      vegMeals: new FormControl(this.vegRes, Validators.required),
      jerseySizes: new FormControl(this.jerseyRes, Validators.required),
      shuttle1: new FormControl(this.shuttleRes),
      shuttle2: new FormControl(this.shuttleRes2),
      shuttle3: new FormControl(this.shuttleRes3),
      routeSelected: new FormControl(this.routeRes)
    });

   

    // Checking logged in state, if they are logged in run regInfo() and getUserInfo() functions from the global dataService.
    if (this.dataService.isLoggedIn() === true && this.dataService.tokenExpired === false) {

      // Get the current flowstep
      this.getFlowStep();

      this.dataService.getParticipationType();

    } else if (this.dataService.storageToken === undefined) {
      this.snackBar.open('Login session expired, please login again.', 'Close', {
        duration: 3500,
        extraClasses: ['error-info']
      });
      this.route.navigate(['/step-01']);

    } else {
      // if not logged in, go back to step 1 (login page)
      this.snackBar.open('You are not logged in, please login.', 'Close', {
        duration: 3500,
        extraClasses: ['error-info']
      });
      this.route.navigate(['/step-01']);
    }

  }

  // Clear the timeout function upon entering a new route
  ngOnDestroy() {
    clearTimeout(this.timeOut);
  }

  ngAfterViewInit() {
    // this.getProfileRes();
  }
  
  // Get the Survey Responses
  getProfileRes() {
    this.method = 'CRTeamraiserAPI?method=getSurveyResponses&api_key=cfrca&v=1.0&fr_id=' + this.dataService.eventID + (this.dataService.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.dataService.ssoToken + '&survey_id=' + this.dataService.surveyID2 + '&cons_email=' + 'baobao@bao.com' + '&cons_first_name=' + 'Bao' + '&cons_last_name=' + 'Mktest' + '&response_format=json';
    this.http.post(this.dataService.convioURL + this.method, null)
      .subscribe(res => {
        this.surveyResults = res;
        console.log(this.surveyResults);

        // For Loop to get Survey Data and set it to the correct variables (to prevent data being saved as undefined or null)
        for (let each of this.surveyResults.getSurveyResponsesResponse.responses) {

          // Jersey
          // if (each.questionId === this.dataService.profile1) {
          //   // console.log(each.responseValue);
          //   if (this.jerseyRes === undefined) {
          //     if (each.responseValue === "User Provided No Response") {
          //       this.jerseyRes = '';
          //     } else {
          //       if (each.responseValue.length > 5) {
          //         let size = each.responseValue.split(" (")[0];  
          //         this.jerseyRes = size
          //         console.log(size);
          //       } else {
          //         this.jerseyRes = each.responseValue;
          //         console.log(this.jerseyRes);
          //       }
          //     }
          //   }
          // }

          // Vegetarian
          if (each.questionId === this.dataService.profile2) {
            if (this.vegRes === undefined) {
              if (each.responseValue === 'User Provided No Response') {
                this.vegRes = '';
              } else if (each.responseValue === 'Yes') {
                this.vegRes = 'Yes';
                console.log(this.vegRes);
              } else if (each.responseValue === 'True') {
                this.vegRes = 'Yes';
                console.log(this.vegRes);
              } else if (each.responseValue === 'No') {
                this.vegRes = 'No';
                console.log(this.vegRes);
              } else if (each.responseValue === 'False') {
                this.vegRes = 'No';
                console.log(this.vegRes);
              }
            }
          }          
        }
      });
      // this.remember === true;
    }
    
  getSurveyRes() {
    // Get the Survey Responses
    this.method = 'CRTeamraiserAPI?method=getSurveyResponses&api_key=cfrca&v=1.0&fr_id=' + this.dataService.eventID + (this.dataService.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.dataService.ssoToken + '&survey_id=' + this.dataService.surveyID + '&response_format=json';
    this.http.post(this.dataService.convioURL + this.method, null)
      .subscribe(res => {
        this.surveyResults = res;
        console.log('getSurveyRes()', this.surveyResults);
        // console.log(this.jerseyRes.length);

        // For Loop to get Survey Data and set it to the correct variables (to prevent data being saved as undefined or null)
        for (let res of this.surveyResults.getSurveyResponsesResponse.responses) {
          

          // Vegetarian Meals
          if (res.questionId === this.dataService.question15) {
            if (res.responseValue === 'Yes' || res.responseValue === 'No') {
              this.vegRes = res.responseValue;
            }
          }

          // How many years have you ridden with The Ride?
          if (res.questionId === this.dataService.question1) {
            if (this.attendanceRes === undefined) {
              this.attendanceRes = '';
            }
            if (res.responseValue !== undefined || res.responseValue !== null) {
              this.attendanceRes = res.responseValue;
            }
            if (Object.keys(res.responseValue).length === 0) {
              this.attendanceRes = '';
            }
          }

          // Jersey Selection
          if (res.questionId === this.dataService.question18) {            
            if (this.jerseyRes === '[object Object]') {
              this.jerseyRes = '';
            }
            if (this.jerseyRes === undefined) {
              this.jerseyRes = '';
            }
            if (res.responseValue !== undefined || res.responseValue !== null) {
              this.jerseyRes = res.responseValue;
            }
            if (Object.keys(res.responseValue).length === 0) {
              this.jerseyRes = '';
            }

          }

          // Shuttle 1 Selection
          if (res.questionId === this.dataService.question20) {
            if (this.shuttleRes === undefined || this.shuttleRes === null) {
              this.shuttleRes = '';
            }
            if (res.responseValue !== undefined || res.responseValue !== null) {
              this.shuttleRes = res.responseValue;
            }
            if (Object.keys(res.responseValue).length === 0) {
              this.shuttleRes = '';
            }
          }

          // Shuttle 2 Selection
          if (res.questionId === this.dataService.question21) {
            if (this.shuttleRes2 === undefined || this.shuttleRes2 === null) {
              this.shuttleRes2 = '';

            }
            if (res.responseValue !== undefined || res.responseValue !== null) {
              this.shuttleRes2 = res.responseValue;
            }
            if (Object.keys(res.responseValue).length === 0) {
              this.shuttleRes2 = '';
            }
          }

          // Shuttle 3 Selection
          if (res.questionId === this.dataService.question22) {
            if (this.shuttleRes3 === undefined || this.shuttleRes3 === null) {
              this.shuttleRes3 = '';
            }
            if (res.responseValue !== undefined || res.responseValue !== null) {
              this.shuttleRes3 = res.responseValue;
            }
            if (Object.keys(res.responseValue).length === 0) {
              this.shuttleRes3 = '';
            }
          }

          // Route Selection
          if (res.questionId === this.dataService.question35) {            
            if (this.routeRes === '[object Object]' || this.routeRes === undefined) {
              this.routeRes = '';
            }
            if (res.responseValue !== undefined || res.responseValue !== null) {
              this.routeRes = res.responseValue;
            }
            if (Object.keys(res.responseValue).length === 0) {
              this.routeRes = '';
            }
          }

          // // Shuttle 4 Selection
          // if (res.questionId === this.dataService.question23) {
          //   if (this.shuttleRes4 === undefined || this.shuttleRes4 === null) {
          //     this.shuttleRes4 = '';
          //   }
          //   if (res.responseValue !== undefined || res.responseValue !== null) {
          //     this.shuttleRes4 = res.responseValue;
          //   }
          //   if (Object.keys(res.responseValue).length === 0) {
          //     this.shuttleRes4 = '';
          //   }
          // }

          // // Shuttle 5 Selection
          // if (res.questionId === this.dataService.question24) {
          //   if (this.shuttleRes5 === undefined || this.shuttleRes5 === null) {
          //     this.shuttleRes5 = '';
          //   }
          //   if (res.responseValue !== undefined || res.responseValue !== null) {
          //     this.shuttleRes5 = res.responseValue;
          //   }
          //   if (Object.keys(res.responseValue).length === 0) {
          //     this.shuttleRes5 = '';
          //   }
          // }

          // // Shuttle 6 Selection
          // if (res.questionId === this.dataService.question25) {
          //   if (this.shuttleRes6 === undefined || this.shuttleRes6 === null) {
          //     this.shuttleRes6 = '';          
          //   }
          //   if (res.responseValue !== undefined || res.responseValue !==  null) {
          //     this.shuttleRes6 = res.responseValue;
          //   }
          //   if (Object.keys(res.responseValue).length === 0) {
          //     this.shuttleRes6 = '';
          //   }
          // }

          // // Shuttle 7 Selection
          // if (res.questionId === this.dataService.question26) {
          //   if (this.shuttleRes7 === undefined || this.shuttleRes7 === null) {
          //     this.shuttleRes7 = '';          
          //   }
          //   if (res.responseValue !== undefined || res.responseValue !==  null) {
          //     this.shuttleRes7 = res.responseValue;
          //   }
          //   if (Object.keys(res.responseValue).length === 0) {
          //     this.shuttleRes7 = '';
          //   }
          // }

          // // Shuttle 8 Selection
          // if (res.questionId === this.dataService.question27) {
          //   if (this.shuttleRes8 === undefined || this.shuttleRes8 === null) {
          //     this.shuttleRes8 = '';          
          //   }
          //   if (res.responseValue !== undefined || res.responseValue !==  null) {
          //     this.shuttleRes8 = res.responseValue;
          //   }
          //   if (Object.keys(res.responseValue).length === 0) {
          //     this.shuttleRes8 = '';
          //   }
          // }

          // // Shuttle 9 Selection
          // if (res.questionId === this.dataService.question28) {
          //   if (this.shuttleRes9 === undefined || this.shuttleRes9 === null) {
          //     this.shuttleRes9 = '';          
          //   }
          //   if (res.responseValue !== undefined || res.responseValue !==  null) {
          //     this.shuttleRes9 = res.responseValue;
          //   }
          //   if (Object.keys(res.responseValue).length === 0) {
          //     this.shuttleRes9 = '';
          //   }
          // }

          // Packet Pickup
          if (res.questionId === this.dataService.question32) {
            if (this.packetRes === undefined || this.packetRes === null) {
              this.packetRes = '';
            }
            if (res.responseValue !== undefined || res.responseValue !==  null) {
              this.packetRes = res.responseValue;
            }
            if (Object.keys(res.responseValue).length === 0) {
              this.packetRes = '';
            }
          }        
        }
      });
    }

  validating() {

    if ( this.dataService.participationType === 'Crew') {
      this.jerseyRes = 'Crew';
      this.packetRes = 'No';
      this.routeRes = 'Crew';
      this.shuttleRes = "I don't need a shuttle";
      this.shuttleRes3 = "I don't need a shuttle";
    }

    if ( this.attendanceRes === 'User Provided No Response' || this.attendanceRes === undefined || this.attendanceRes === 'undefined' || this.attendanceRes === '[object Object]' || this.attendanceRes.length === 0 ) {
      this.yearError = true;
    } else {
      this.yearError = false;
    }
    if ( this.jerseyRes === 'User Provided No Response' || this.jerseyRes === undefined || this.jerseyRes === 'undefined' || this.jerseyRes === '[object Object]' || this.jerseyRes.length === 0 ) {
      this.jerseyError = true;
    } else {
      this.jerseyError = false;
    }
    if ( this.routeRes === 'User Provided No Response' || this.routeRes === undefined || this.routeRes === 'undefined' || this.routeRes === '[object Object]' || this.routeRes.length === 0) {
      this.routeError = true;
    } else {
      this.routeError = false;
    }
    if ( this.vegRes === 'User Provided No Response' || this.vegRes === undefined || this.vegRes === 'undefined' || this.vegRes === '[object Object]' || this.vegRes.length === 0) {
      this.vegError = true;
    } else {
      this.vegError = false;
    }
    if ( this.shuttleRes === 'User Provided No Response' || this.shuttleRes === undefined || this.shuttleRes === 'undefined' || this.shuttleRes === '[object Object]' || this.shuttleRes.length === 0) {
      this.shuttle1Error = true;
    } else {
      this.shuttle1Error = false;
    }
    if ( this.shuttleRes2 === 'User Provided No Response' || this.shuttleRes2 === undefined || this.shuttleRes2 === 'undefined' || this.shuttleRes2 === '[object Object]' || this.shuttleRes2.length === 0) {
      this.shuttle2Error = true;
    } else {
      this.shuttle2Error = false;
    }
    if ( this.shuttleRes3 === 'User Provided No Response' || this.shuttleRes3 === undefined || this.shuttleRes3 === 'undefined' || this.shuttleRes3 === '[object Object]' || this.shuttleRes3.length === 0) {
      this.shuttle3Error = true;
    } else {
      this.shuttle3Error = false;
    }
    if ( this.packetRes === 'User Provided No Response' || this.packetRes === undefined || this.packetRes === 'undefined' || this.packetRes === '[object Object]'  || this.packetRes.length === 0) {
      this.packetError = true;
    } else {
      this.packetError = false;
    }
    // if ( this.shuttleRes4 === 'User Provided No Response' || this.shuttleRes4 === undefined || this.shuttleRes4 === 'undefined' || this.shuttleRes4 === '[object Object]') {
    //   this.shuttle4Error = true;
    // } else {
    //   this.shuttle4Error = false;
    // }
    // if ( this.shuttleRes5 === 'User Provided No Response' || this.shuttleRes5 === undefined || this.shuttleRes5 === 'undefined' || this.shuttleRes5 === '[object Object]' ) {
    //   this.shuttle5Error = true;
    // } else {
    //   this.shuttle5Error = false;
    // }
    // if ( this.shuttleRes6 === 'User Provided No Response' || this.shuttleRes6 === undefined || this.shuttleRes6 === 'undefined' || this.shuttleRes6 === '[object Object]' ) {
    //   this.shuttle6Error = true;
    // } else {
    //   this.shuttle6Error = false;
    // }
    // if ( this.shuttleRes7 === 'User Provided No Response' || this.shuttleRes7 === undefined || this.shuttleRes7 === 'undefined' || this.shuttleRes7 === '[object Object]' ) {
    //   this.shuttle7Error = true;
    // } else {
    //   this.shuttle7Error = false;
    // }
    // if ( this.shuttleRes8 === 'User Provided No Response' || this.shuttleRes8 === undefined || this.shuttleRes8 === 'undefined' || this.shuttleRes8 === '[object Object]' ) {
    //   this.shuttle8Error = true;
    // } else {
    //   this.shuttle8Error = false;
    // }
    // if ( this.shuttleRes9 === 'User Provided No Response' || this.shuttleRes9 === undefined || this.shuttleRes9 === 'undefined' || this.shuttleRes9 === '[object Object]' ) {
    //   this.shuttle9Error = true;
    // } else {
    //   this.shuttle9Error = false;
    // }    

    // if ( this.yearError ||this.jerseyError || this.vegError || this.shuttle1Error || this.shuttle2Error || this.shuttle3Error || this.shuttle4Error || this.shuttle5Error || this.shuttle6Error || this.shuttle7Error || this.shuttle8Error || this.shuttle9Error || this.packetError ) {
    //   console.log('Error spotted')
    // } else {
    //   this.updateSurveyRes();
    // }

    if ( this.yearError ||this.jerseyError || this.vegError || this.shuttle1Error || this.shuttle2Error || this.shuttle3Error || this.routeError || this.packetError ) {
      console.log('Error spotted')
    } else {
      this.updateSurveyRes();
    }

  }

  

  // Update the Survey Responses
  updateSurveyRes() {

    for (const resp of this.surveyResults.getSurveyResponsesResponse.responses) {      

      // Vegetarian Meals
      if (resp.questionId === this.dataService.question15) {
        if (this.vegRes === undefined) {
          this.vegRes = resp.responseValue;
        }
      }

      // How many years have you ridden with The Ride?
      if (resp.questionId === this.dataService.question1) {
        if (this.attendanceRes === undefined || this.attendanceRes === null) {
          this.attendanceRes = resp.responseValue;
        }
      }

      // Jersey Selection
      if (resp.questionId === this.dataService.question18) {
        if (this.jerseyRes === undefined || this.jerseyRes === null) {
          this.jerseyRes = resp.responseValue;
        }
      }

      // Shuttle 1 Selection
      if (resp.questionId === this.dataService.question20) {
        if (this.shuttleRes === undefined || this.shuttleRes === null) {
          this.shuttleRes = resp.responseValue;
        }
      }

      // Shuttle 2 Selection
      if (resp.questionId === this.dataService.question21) {
        if (this.shuttleRes2 === undefined || this.shuttleRes2 === null) {
          this.shuttleRes2 = resp.responseValue;
        }
      }

      // Shuttle 3 Selection
      if (resp.questionId === this.dataService.question22) {
        if (this.shuttleRes3 === undefined || this.shuttleRes3 ===  null) {
          this.shuttleRes3 = resp.responseValue;
        }
      }

      // Route Selection
      if (resp.questionId === this.dataService.question35) {            
        if (this.shuttleRes3 === undefined || this.shuttleRes3 ===  null) {
          this.routeRes = resp.responseValue;
        }
      }

      // // Shuttle 4 Selection
      // if (resp.questionId === this.dataService.question23) {
      //   if (this.shuttleRes4 === undefined || this.shuttleRes4 === null) {
      //     this.shuttleRes4 = resp.responseValue;
      //   }
      // }

      // // Shuttle 5 Selection
      // if (resp.questionId === this.dataService.question24) {
      //   if (this.shuttleRes5 === undefined || this.shuttleRes5 ===  null) {
      //     this.shuttleRes5 = resp.responseValue;
      //   }
      // }

      // // Shuttle 6 Selection
      // if (resp.questionId === this.dataService.question25) {
      //   if (this.shuttleRes6 === undefined || this.shuttleRes6 === null) {
      //     this.shuttleRes6 = resp.responseValue;
      //   }
      // }

      // // Shuttle 7 Selection
      // if (resp.questionId === this.dataService.question26) {
      //   if (this.shuttleRes7 === undefined || this.shuttleRes7 === null) {
      //     this.shuttleRes7 = resp.responseValue;
      //   }
      // }

      // // Shuttle 8 Selection
      // if (resp.questionId === this.dataService.question27) {
      //   if (this.shuttleRes8 === undefined || this.shuttleRes8 ===  null) {
      //     this.shuttleRes8 = resp.responseValue;
      //   }
      // }

      // // Shuttle 9 Selection
      // if (resp.questionId === this.dataService.question28) {
      //   if (this.shuttleRes9 === undefined || this.shuttleRes9 === null) {
      //     this.shuttleRes9 = resp.responseValue;
      //   }
      // }


      // Packet Pickup
      if (resp.questionId === this.dataService.question32) {
        if (this.packetRes === undefined || this.packetRes === null) {
          this.packetRes = resp.responseValue;
        }
      }      

    }

    const updateSurveyResponsesUrl = 'https://secure.conquercancer.ca/site/CRTeamraiserAPI?method=updateSurveyResponses&api_key=cfrca&v=1.0&response_format=json&fr_id='+ this.dataService.eventID + '&survey_id=' + this.dataService.surveyID;

    const question_attendance = '&question_' + this.dataService.question1 + '=' + this.attendanceRes;
    const question_veg = '&question_'+ this.dataService.question15 + '=' + this.vegRes;
    const question_jersey = '&question_' + this.dataService.question18 + '=' + this.jerseyRes;
    const question_shuttle1 = '&question_' + this.dataService.question20 + '=' + this.shuttleRes;
    const question_shuttle2 = '&question_' + this.dataService.question21 + '=' + this.shuttleRes2;
    const question_shuttle3 = '&question_' + this.dataService.question22 + '=' + this.shuttleRes3;
    const question_route = '&question_' + this.dataService.question35 + '=' + this.routeRes;
    // const question_shuttle4 = '&question_' + this.dataService.question23 + '=' + this.shuttleRes4;
    // const question_shuttle5 = '&question_' + this.dataService.question24 + '=' + this.shuttleRes5;
    // const question_shuttle6 = '&question_' + this.dataService.question25 + '=' + this.shuttleRes6;
    // const question_shuttle7 = '&question_' + this.dataService.question26 + '=' + this.shuttleRes7;
    // const question_shuttle8 = '&question_' + this.dataService.question27 + '=' + this.shuttleRes8;
    // const question_shuttle9 = '&question_' + this.dataService.question28 + '=' + this.shuttleRes9;
    const question_packet = '&question_' + this.dataService.question32 + '=' + this.packetRes;

    this.http.post(updateSurveyResponsesUrl + question_attendance  + question_veg + question_jersey + question_shuttle1 + question_shuttle2 + question_shuttle3 + question_route + question_packet + (this.dataService.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.dataService.ssoToken, null)
      .subscribe(res => {
          console.log(res);
          this.updateReg();
          this.route.navigate(['/step-04']);
        },
        (error) => {
          console.log(error);
          this.route.navigate(['/step-01']);
        });
  }

  // Save Current Survey Answers (save for later)
  saveSurveyRes() {

    const updateSurveyResponsesUrl = 'https://secure.conquercancer.ca/site/CRTeamraiserAPI?method=updateSurveyResponses&api_key=cfrca&v=1.0&response_format=json&fr_id='+ this.dataService.eventID + '&survey_id=' + this.dataService.surveyID;

    const question_attendance = '&question_' + this.dataService.question1 + '=' + this.attendanceRes;
    const question_veg = '&question_'+ this.dataService.question15 + '=' + this.vegRes;
    const question_jersey = '&question_' + this.dataService.question18 + '=' + this.jerseyRes;
    const question_shuttle1 = '&question_' + this.dataService.question20 + '=' + this.shuttleRes;
    const question_shuttle2 = '&question_' + this.dataService.question21 + '=' + this.shuttleRes2;
    const question_shuttle3 = '&question_' + this.dataService.question22 + '=' + this.shuttleRes3;
    const question_route = '&question_' + this.dataService.question35 + '=' + this.routeRes;
    // const question_shuttle4 = '&question_' + this.dataService.question23 + '=' + this.shuttleRes4;
    // const question_shuttle5 = '&question_' + this.dataService.question24 + '=' + this.shuttleRes5;
    // const question_shuttle6 = '&question_' + this.dataService.question25 + '=' + this.shuttleRes6;
    // const question_shuttle7 = '&question_' + this.dataService.question26 + '=' + this.shuttleRes7;
    // const question_shuttle8 = '&question_' + this.dataService.question27 + '=' + this.shuttleRes8;
    // const question_shuttle9 = '&question_' + this.dataService.question28 + '=' + this.shuttleRes9;
    const question_packet = '&question_' + this.dataService.question32 + '=' + this.packetRes;

    this.http.post(updateSurveyResponsesUrl + question_attendance  + question_veg + question_jersey + question_shuttle1 + question_shuttle2 + question_shuttle3 + question_route + question_packet + (this.dataService.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.dataService.ssoToken, null)
      .subscribe(res => {
          this.saveUpdateReg();
        },
        (error) => {
          console.log(error);
          this.snackBar.open('There was an error while trying to save. Please check the form.', 'Close', {
            duration: 3500,
            extraClasses: ['error-info']
          });
          this.route.navigate(['/step-01']);
        });
  }

  // Update the Registration Information
  updateReg() {

    this.flowStep = '3';

    const paidStatus = this.dataService.checkInStatus === 'Paid';
    const completeStatus = this.dataService.checkInStatus === 'Complete';
    const committedStatus = this.dataService.checkInStatus === 'Committed';

    if (paidStatus || completeStatus || committedStatus) {
      this.method = 'CRTeamraiserAPI?method=updateRegistration&api_key=cfrca&v=1.0' + '&fr_id=' + this.dataService.eventID + (this.dataService.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.dataService.ssoToken + '&checkin_status=' + this.dataService.checkInStatus + '&flow_step=' + this.flowStep + '&response_format=json';

    } else {
      this.method = 'CRTeamraiserAPI?method=updateRegistration&api_key=cfrca&v=1.0' + '&fr_id=' + this.dataService.eventID + (this.dataService.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.dataService.ssoToken + '&flow_step=' + this.flowStep + '&response_format=json';
    }


    this.http.post(this.dataService.convioURL + this.method, null)
      .subscribe(res => {
        this.snackBar.open('Your information has been saved!', 'Close', {
          duration: 3500,
          extraClasses: ['saved-info']
        });
      }, (error) => {
        if (error) {
          this.snackBar.open('Sorry, there was an error, please try again.', 'Close', {
            duration: 3500,
            extraClasses: ['error-info']
          });
        }
      });
  }

  // Update the Registration Information
  saveUpdateReg() {

    this.flowStep = '3';

    const paidStatus = this.dataService.checkInStatus === 'Paid';
    const completeStatus = this.dataService.checkInStatus === 'Complete';
    const committedStatus = this.dataService.checkInStatus === 'Committed';

    if (paidStatus || completeStatus || committedStatus) {
      this.method = 'CRTeamraiserAPI?method=updateRegistration&api_key=cfrca&v=1.0' + '&fr_id=' + this.dataService.eventID + (this.dataService.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.dataService.storageToken + '&checkin_status=' + this.dataService.checkInStatus + '&flow_step=' + this.flowStep + '&response_format=json';
    } else {
      this.method = 'CRTeamraiserAPI?method=updateRegistration&api_key=cfrca&v=1.0' + '&fr_id=' + this.dataService.eventID + (this.dataService.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.dataService.storageToken + '&flow_step=' + this.flowStep + '&response_format=json';
    }

    this.http.post(this.dataService.convioURL + this.method, null)
      .subscribe(res => {
        this.snackBar.open('Your information has been saved!', 'Close', {
          duration: 3500,
          extraClasses: ['saved-info']
        });
      }, (error) => {
        if (error) {
          this.snackBar.open('Sorry, there was an error, please try again.', 'Close', {
            duration: 3500,
            extraClasses: ['error-info']
          });
        }
      });
  }

  // Get the current Flowstep
  getFlowStep() {
    const token = localStorage.getItem('token');
    this.method = 'CRTeamraiserAPI?method=getFlowStep&api_key=cfrca&v=1.0&response_format=json&fr_id=' + this.dataService.eventID + (this.dataService.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + token;
    this.http.post(this.dataService.convioURL + this.method, null)
      .subscribe(res => {
        this.flowStepResults = res;
        this.getFlowStepNumber = this.flowStepResults.getFlowStepResponse.flowStep;

        // Checking the participants flow step to prevent user from skipping a flowstep
        if (this.getFlowStepNumber === this.flowStep) {
          // If the flow step matches to where they are supposed to be, then run the functions for the page below
          this.dataService.getRegInfo();
          this.getSurveyRes();

        } else {
          // If flowstep does not match, show error message and send them back to the previous page/flowstep.

          this.snackBar.open('You have been redirected to your previously saved location.', 'Close', {
            duration: 3500,
            extraClasses: ['routing-info']
          });

          // Check the Flowstep, if matched, send them to the proper route
          if (this.getFlowStepNumber === '0') {
            this.route.navigate(['/step-02']);
          }
          if (this.getFlowStepNumber === '1') {
            this.route.navigate(['/step-02']);
          }
          if (this.getFlowStepNumber === '2') {
            this.route.navigate(['/step-03']);
          }
          if (this.getFlowStepNumber === '3') {
            this.route.navigate(['/step-04']);
          }
          if (this.getFlowStepNumber === '4') {
            this.route.navigate(['/step-05']);
          }
          if (this.getFlowStepNumber === '5') {
            this.route.navigate(['/step-06']);
          }
          if (this.getFlowStepNumber === '6') {
            this.route.navigate(['/step-07']);
          }
          if (this.getFlowStepNumber === '7') {
            this.route.navigate(['/step-08']);
          }
          if (this.getFlowStepNumber === '8') {
            this.route.navigate(['/step-09']);
          }
        }

      }, (err) => {
        // console.log(err);
        this.snackBar.open('There was an error, please login again.', 'Close', {
          duration: 3500,
          extraClasses: ['error-info']
        });
        this.dataService.logOut();
      });
  }

  // Update the current Flowstep
  previousFlowStep() {
    // this.flowStep = '1';

    const paidStatus = this.dataService.checkInStatus === 'Paid';
    const completeStatus = this.dataService.checkInStatus === 'Complete';
    const committedStatus = this.dataService.checkInStatus === 'Committed';

    if (paidStatus || completeStatus || committedStatus) {
      this.dataService.method = 'CRTeamraiserAPI?method=updateRegistration&api_key=cfrca&v=1.0' + '&fr_id=' + this.dataService.eventID + (this.dataService.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.dataService.ssoToken + '&checkin_status=' + this.dataService.checkInStatus + '&flow_step=1' + '&response_format=json';
    } else {
      this.dataService.method = 'CRTeamraiserAPI?method=updateRegistration&api_key=cfrca&v=1.0' + '&fr_id=' + this.dataService.eventID + (this.dataService.isManualLogin === true ? '&sso_auth_token=' : '&auth=') + this.dataService.ssoToken + '&flow_step=1' + '&response_format=json';
    }


    this.http.post(this.dataService.convioURL + this.dataService.method, null)
      .subscribe(res => {
        // console.log('Flow step updated.')
        this.route.navigate(['/step-02']);
      }, (err) => {
        if (err) {
            this.snackBar.open('There was an unknown error.', 'Close', {
              duration: 3500,
              extraClasses: ['error-info']
            });
          this.dataService.logOut();
        }
      });
  }

}
